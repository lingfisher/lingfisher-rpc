const ffmpeg = require("fluent-ffmpeg");
const ps = require("pocketsphinx").ps;

const modelDir = "data/pocketsphinx/en-us";

const config = new ps.Decoder.defaultConfig();
config.setString("-hmm", `${modelDir}/hmm`);
config.setString("-lm", `${modelDir}/en-70k-0.2.lm`);
config.setString("-dict", `${modelDir}/cmudict-en-us.dict`);
config.setString("-logfn", "/dev/null");  // turn off debug logs
const decoder = new ps.Decoder(config);

const FORMAT_MAPPING = {
  weba: "webm",
  oga: "ogg"
};

const recognize = (audio, extension) => {
  const format = FORMAT_MAPPING[extension] ? FORMAT_MAPPING[extension] : extension;

  return new Promise((resolve, reject) => {
    const convert = ffmpeg(audio)
      .inputFormat(format)
      .audioFrequency(16000)
      .audioChannels(1)
      .format("wav")
      .on("error", (err) => {
        reject(err);
      })
      .on("start", () => {
        process.stdout.write("Started conversion\n");
      })
      .on("end", () => {
        process.stdout.write("Finished conversion\n");
      });

    const chunks = [];

    const ffStream = convert.pipe({ end: true });

    ffStream.on("error", err => {
      reject(err);
    });

    ffStream.on("data", chunk => {
      chunks.push(chunk);
    });

    ffStream.on("end", () => {
      decoder.startUtt();
      decoder.processRaw(Buffer.concat(chunks), false, false);
      decoder.endUtt();
      resolve(decoder.hyp());
    });
  });
};

module.exports = recognize;
