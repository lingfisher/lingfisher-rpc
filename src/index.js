const amqp = require("amqplib");
const streamifier = require("streamifier");

const recognize = require("./recognize");

const AMQP_URL = process.env.CLOUDAMQP_URL || "amqp://localhost";

amqp.connect(AMQP_URL).then((conn) => {
  process.once("SIGINT", function() { conn.close(); });
  return conn.createChannel().then((ch) => {
    const q = "rpc_queue";
    let ok = ch.assertQueue(q, { durable: false });
    ok = ok.then(function() {
      ch.prefetch(1);
      return ch.consume(q, reply);
    });
    return ok.then(function() {
      process.stdout.write(" [x] Awaiting RPC requests\n");
    });

    function reply(msg) {
      const payload = JSON.parse(msg.content);
      const audio = streamifier.createReadStream(Buffer.from(payload.audio));
      const extension = payload.extension;

      process.stdout.write(" [.] Recognizing input\n");
      recognize(audio, extension)
        .then((response) => {      
          ch.sendToQueue(msg.properties.replyTo,
            Buffer.from(JSON.stringify(response)),
            { correlationId: msg.properties.correlationId });
          ch.ack(msg);
          process.stdout.write(" [*] Done \n");
        });
    }
  });
}).catch(console.warn);
